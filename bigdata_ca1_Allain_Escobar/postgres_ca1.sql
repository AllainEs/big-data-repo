--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 9.6.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE comments (
    username text,
    comment text,
    post integer
);


ALTER TABLE comments OWNER TO postgres;

--
-- Name: movies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE movies (
    title text NOT NULL,
    writer text,
    year smallint,
    actors text[],
    franchise text,
    sypnosis text
);


ALTER TABLE movies OWNER TO postgres;

SET default_with_oids = true;

--
-- Name: posts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE posts (
    username text,
    title text,
    body text
);


ALTER TABLE posts OWNER TO postgres;

SET default_with_oids = false;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    username text NOT NULL,
    first_name text,
    last_name text
);


ALTER TABLE users OWNER TO postgres;

--
-- Data for Name: comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY comments (username, comment, post) FROM stdin;
GoodGuyGreg	Hope you got a good deal!	16764
GoodGuyGreg	What's mine is yours!	16765
GoodGuyGreg	Don't violate the licensing agreement	16766
ScumbagSteve	It still isn't clean	16761
ScumbagSteve	Denied your PR cause I found a hack	16763
\.


--
-- Data for Name: movies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY movies (title, writer, year, actors, franchise, sypnosis) FROM stdin;
Fight Club	Chuck Palahniuk	1999	{"Brad Pitt","Edward Norton"}	\N	\N
Inglorious Basterds	Quentin Tarantino	2009	{"Brad Pitt","Diane Kruger","Eli Roth"}	\N	\N
The Hobbit: The Battle of the Five Armies	J.R.R Tolkein	2012	\N	The Hobbit	Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness.
The Hobbit: An Unexpected Journey	J.R.R Tolkein	2012	\N	The Hobbit	A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug.
The Hobbit: The Desolation of Smaug	J.R.R Tolkein	2013	\N	The Hobbit	The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring.
Pulp Fiction	Quentin Tarantino	1994	{"John Travolta","Uma Thurman","Samuel L. Jackson"}	\N	\N
\.


--
-- Data for Name: posts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY posts (username, title, body) FROM stdin;
GoodGuyGreg	Passes out at party	Wakes up early and cleans house
GoodGuyGreg	Steals your identity	Raises your credit score
GoodGuyGreg	Reports a bug in your code	Sends a pull request
ScumbagSteve	Borrows something	Sells it
ScumbagSteve	Borrows everything	The end
ScumbagSteve	Forks your repo on github	Sets to private
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (username, first_name, last_name) FROM stdin;
GoodGuyGreg	Good Guy	Greg
ScumbagSteve	Scumbag	Steve
\.


--
-- Name: movies movies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY movies
    ADD CONSTRAINT movies_pkey PRIMARY KEY (title);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (username);


--
-- Name: comments comments_username_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_username_fkey FOREIGN KEY (username) REFERENCES users(username);


--
-- Name: posts posts_username_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_username_fkey FOREIGN KEY (username) REFERENCES users(username) MATCH FULL;


--
-- PostgreSQL database dump complete
--

