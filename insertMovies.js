/******************************QUESTION 2*****************************************/
db.movies.insertMany([{
	title :"Fight Club",
	writer : "Chuck Palahniuk" ,
	year : 1999,
	actors : ["Brad Pitt" ,"Edward Norton" ]
},{
	title:"Pulp Fiction",
	writer:"Quentin Tarantino",
	year:1994,
	actors:["John Travolta","Uma Thurman"]	
},{
	title : "Inglorious Basterds",
	writer : "Quentin Tarantino", 
	year : 2009 ,
	actors : ["Brad Pitt","Diane Kruger","Eli Roth"]	
},{
	title : "The Hobbit: An Unexpected Journey",
	writer : "J.R.R. Tolkein", 
	year : 2012 ,
	franchise : "The Hobbit"
},{
	title : "The Hobbit: The Desolation of Smaug", 
	writer : "J.R.R. Tolkein", 
	year : 2013,
	franchise : "The Hobbit"
},{
	title : "The Hobbit: An Unexpected Journey",
	writer : "J.R.R. Tolkein" ,
	year : 2012 ,
	franchise : "The Hobbit"
},{
	title : "The Hobbit: The Battle of the Five Armies", 
	writer : "J.R.R. Tolkein",
	year : 2012 ,
	franchise : "The Hobbit", 
	synopsis : "Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness."
},{
	title : "Pee Wee Herman's Big Adventure"
},{
	title : "Avatar"
}]);

/********************************************QUESTION 3 FINDS DOCUMENTS***********************************/
db.movies.find()//1.Get All Documents
db.movies.find({writer:"Quentin Tarantino"}).pretty();//2.get all documents with `writer` set to "Quentin Tarantino" 
db.movies.find({actors:"Brad Pitt"}).pretty();//get all documents where `actors` include "Brad Pitt" 
db.movies.find({franchise:"The Hobbit"}).pretty();//get all documents with `franchise` set to "The Hobbit" 
db.movies.find({$and: [{year: {$gt: 1900}}, {year: {$lt: 2000}}]}) //get all movies released in the 90s 
db.movies.find({$or: [{year: {$lt: 2000}}, {year: {$gt: 2010}}]})//. get all movies released before the year 2000 or after 2010

/***************************************QUESTION 4 UPDATE DOCUMENTS*************************************************/
/*1*/db.movies.update({title: "The Hobbit: An Unexpected Journey"}, {$push:{synopsis: "A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug."}})
/*2*/db.movies.update({title: "The Hobbit: The Desolation of Smaug"},{$push:{sypnosis:"The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring"}});
/*2*/db.movies.update({title: "Pulp Fiction"},{$push:{actors:"Samuel L. Jackson"}});
/***************************************QUESTION 5 TEXT SEARCH*************************************************/
/*create index*/ db.movies.createIndex({sypnosis:"text"});
/*1*/db.movies.find({"$text":{"$search":"Bilbo"}}).pretty();
/*2*/db.movies.find({"$text":{"$search":"Gandalf"}}).pretty();
/*3*/db.movies.find({"$text":{"$search":"Bilbo -Gandalf"}}).pretty();
/*4*/db.movies.find({"$text":{"$search":"dwarves" "hobbit"}}).pretty();
/*5*/db.movies.find({"$text":{"$search":"\"gold\" \"dragon\""}}).pretty()
/***********************************QUESTION 6 DELETE DOCUMENT************************************************/
db.movies.remove({title:"Pee Wee Herman's Big Adventure"});
db.movies.remove({title:"Avatar"});
/**********************************QUESTION 7 RELATIONSHIPS****************************************************/
db.users.insert({
	username : "GoodGuyGreg",
	first_name : "Good Guy", 
	last_name : "Greg"
});

db.users.insert({
	username : "ScumbagSteve",
	first_name : "Scumbag",
	last_name : "Steve"

});
/**********************************POSTS COMMENTS****************************************************/
db.posts.insertMany([{
	username : "GoodGuyGreg",
	title : "Passes out at party",
	body : "Wakes up early and cleans house"
},{
	username : "GoodGuyGreg",
	title : "Steals your identity",
	body : "Raises your credit score"
},{
	username : "GoodGuyGreg",
	title : "Reporst a bug in your code",
	body : "Sends you a pull request"
},{
	username : "GoodGuyGreg",
	title : "Passes out at party",
	body : "Wakes up early and cleans house"
},{
	username : "ScumbagSteve",
	title : "Borrows Something",
	body : "Sells it"
},{
	username : "ScumbagSteve",
	title : "Borrows Everything",
	body : "The end"
},{
	username : "ScumbagSteve",
	title : "Forks your repo on github",
	body : "Wakes up early and cleans house"
}]);
/**********************************QUESTION 8 RELATIONSHIPS****************************************************/
db.comments.insertMany([
{
	username: "GoodGuyGreg",
	comment: "Hope you got a good deal!",
	post :  ObjectId("59f1d57f6d51d31cafec9753")	
},{
	username: "GoodGuyGreg",
	comment: "What's mine is yours!",
	post:  ObjectId("59f1d57f6d51d31cafec9754")
},{
	username: "GoodGuyGreg",
	comment: "Don't violate the license agreement!",
	post: ObjectId("59f1d57f6d51d31cafec9755")
},{
	username: "ScumbagSteve",
	comment: "It still isn't cleaned",
	post:  ObjectId("59f1d57f6d51d31cafec974f")
},{
	username:"ScumbagSteve",
	comment: "Denied your PR cause I found a hack",
	post: ObjectId("59f1d57f6d51d31cafec9751")
}]);
/**********************************QUESTION 9 FIND RELATIONSHIPS****************************************************/
db.users.find().pretty();
db.posts.find().pretty();
db.posts.find({username:"GoodGuyGreg"}).pretty();
db.posts.find({username:"ScumbagSteve"}).pretty();
db.comments.find().pretty();
db.comments.find({username:"GoodGuyGreg"}).pretty();
db.comments.find({username:"ScumbagSteve"}).pretty();
db.comments.find({post: ObjectId("59f1d57f6d51d31cafec9751")}).pretty()
/*******************************************************************************************************************************
********************************************************************************************************************************
********************************************************************************************************************************/
/*
******************************************POSTGRESQL****************************************************************************
*/
insert into movies(title,writer,year,actors,franchise,synopsis) values ('Fight Club','Chuck Palahniuk',1999,ARRAY['Brad Pitt','Edward Norton'],NULL,NULL),
('Pulp Fiction','Quentin Tarantino',1994,ARRAY['John Travolta','Uma Thurman'],NULL,NULL),
('Inglorious Basterds','Quentin Tarantino',2009,ARRAY['Brad Pitt','Diane Kruger','Eli Roth'],NULL,NULL),
('The Hobbit: An Unexpected Journey','J.R.R Tolkein', 2012,NULL,'The Hobbit',NULL),
('The Hobbit:THe Desolation of Smaug','J.R.R Tolkein',2013,NULL,'The Hobbit',NULL),
('The Hobbit: The Battle of the Five Armies', 'J.R.R Tolkein', 2012,NULL,'The Hobbit','Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness.'),
('Pee Wee Herman''s  Big Adventure',NULL,NULL,NULL,NULL,NULL),
('Avatar',NULL,NULL,NULL,NULL,NULL);

/**********************************QUESTION 3 FIND DOCUMENTS****************************************************/
select * from movies;
select * from movies where writer = 'Quentin Tarantino';
select * from movies where actors @> '{Brad Pitt}'::text[];
select * from movies where franchise = 'The Hobbit';
select * from  movies where year > 1900 and year < 2000;
select * from movies where year < 2000 or year > 2010;
/**********************************QUESTION 4 UPDATE DOCUMENTS****************************************************/
update movies set sypnosis = 'A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug.'
where title = (select title from movies where title = 'The Hobbit: An Unexpected Journey');
update movies set sypnosis = 'The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring.' 
where title = (select title from movies where title = 'The Hobbit:THe Desolation of Smaug');
update movies set actors = '{Samuel L. Jackson}' where title = (select title from movies where title = 'Pulp Fiction');
/**********************************QUESTION 5 TEXT SEARCH****************************************************/
 select title from movies where to_tsvector(sypnosis) @@ to_tsquery('english','Gandalf');
 select title from movies where to_tsvector(sypnosis) @@ to_tsquery('english','Bilbo');
 select title from movies where to_tsvector(sypnosis) @@ to_tsquery('english','Bilbo & !Gandalf');
 select title from movies where to_tsvector(sypnosis) @@ to_tsquery('english','dwarves | hobbit');
 select title from movies where to_tsvector(sypnosis) @@ to_tsquery('english','gold & dragon');
 
 select title from movies where sypnosis @@ 'Bilbo';
 select title from movies where sypnosis @@ 'Gandalf';
 select title from movies where sypnosis @@ 'Bilbo & !Gandalf';
 select title from movies where sypnosis @@ 'dwarves | hobbit';
 select title from movies where sypnosis @@ 'gold & dragon';
 /**********************************QUESTION 6 DELETE DOCUMENTS****************************************************/
 delete from movies where title = 'Pee Wee Herman''s Big Adventure';
 delete from movies where title = 'Avatar';
 /**********************************QUESTION 7 RELATIONSHIPS******************************************************/
insert into users(username,first_name,last_name) values('GoodGuyGreg','Good Guy','Greg'),('ScumbagSteve','Scumbag','Steve');
insert into posts(username,title,body) values('GoodGuyGreg','Passes out at party','Wakes up early and cleans house'),
('GoodGuyGreg','Steals your identity','Raises your credit score'),
('GoodGuyGreg','Reports a bug in your code','Sends a pull request'),
('ScumbagSteve','Borrows something','Sells it'),
('ScumbagSteve','Borrows everything','The end'),
('ScumbagSteve','Forks your repo on github','Sets to private');
insert into comments(username,comment,post) values('GoodGuyGreg','Hope you got a good deal!',16764),
('GoodGuyGreg','What''s mine is yours!',16765),
('GoodGuyGreg','Don''t violate the licensing agreement',16766),
('ScumbagSteve','It still isn''t clean',16761),
('ScumbagSteve','Denied your PR cause I found a hack',16763);
/**********************************QUESTION 8******************************************************/
select * from users;
select * from posts;
select * from posts where to_tsvector(username) @@ to_tsquery('GoodGuyGreg');
select * from posts where to_tsvector(username) @@ to_tsquery('ScumbagSteve');
select * from comments;
select comment from comments where to_tsvector(username) @@ to_tsquery('GoodGuyGreg');
select comment from comments where to_tsvector(username) @@ to_tsquery('ScumbagSteve');
select comment from comments where oid = 16763;


 
 
 
 
 
 